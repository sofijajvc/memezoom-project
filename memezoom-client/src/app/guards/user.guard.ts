import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable({ providedIn: 'root' })
export class UserGuard implements CanActivate {

    constructor(private router: Router, private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    	if (this.userService.isLoggedIn()) {
    		const username = route.paramMap.get('username');
            if (username == this.userService.getCurrentUser().username) {
                return true;
            } else {
                this.router.navigate(['/forbidden']);
                return false;
            }
    	} else {
    		this.router.navigate(['/forbidden']);
    		return false;
    	}
    }
}