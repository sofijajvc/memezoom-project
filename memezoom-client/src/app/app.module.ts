import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { MatDialogModule } from '@angular/material';
import { MatCardModule } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { LoginDialogComponent } from './components/login-dialog/login-dialog.component';
import { RegisterDialogComponent } from './components/register-dialog/register-dialog.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { MemeCardComponent } from './components/memes/meme-card/meme-card.component';
import { MemePageComponent } from './components/memes/meme-page/meme-page.component';
import { CommentDialogComponent } from './components/memes/comment-dialog/comment-dialog.component';
import { UploadDialogComponent } from './components/user/upload-dialog/upload-dialog.component';
import { ReportsComponent } from './components/admin/reports/reports.component';
import { RegistrationsComponent } from './components/admin/registrations/registrations.component';
import { SearchResultComponent } from './components/search-result/search-result.component';
import { MemeGridComponent } from './components/memes/meme-grid/meme-grid.component';
import { ForbiddenAccessComponent } from './components/forbidden-access/forbidden-access.component';
import { ReportDialogComponent } from './components/user/report-dialog/report-dialog.component';
import { InboxComponent } from './components/user/inbox/inbox.component';
import { MessageDialogComponent } from './components/user/message-dialog/message-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    HomeComponent,
    LoginDialogComponent,
    RegisterDialogComponent,
    ProfileComponent,
    MemeCardComponent,
    MemePageComponent,
    CommentDialogComponent,
    UploadDialogComponent,
    ReportsComponent,
    RegistrationsComponent,
    SearchResultComponent,
    MemeGridComponent,
    ForbiddenAccessComponent,
    ReportDialogComponent,
    InboxComponent,
    MessageDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatCardModule,
    MatTabsModule,
    MatTableModule,
    MatGridListModule,
    MatPaginatorModule  ],
  entryComponents: [
    LoginDialogComponent,
    RegisterDialogComponent,
    CommentDialogComponent,
    UploadDialogComponent,
    ReportDialogComponent,
    MessageDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
