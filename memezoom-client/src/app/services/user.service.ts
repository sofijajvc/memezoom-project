import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User, Message } from '../models/user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  server: string = 'http://localhost:8000/';

  constructor(private http: HttpClient, private router: Router) { }

  getUser(username: string) {
    return this.http.get<User>(this.server + 'user/' + username);
  }

  getCurrentUser() {
    var currentUserJSON = localStorage.getItem('currentUser');
    if (!currentUserJSON) {
      return null;
    }

    return JSON.parse(currentUserJSON);
  }

  register(username: string, name: string, email: string, password: string, file: File) {
    var formData: FormData = new FormData();
    formData.append('image', file, file.name);
    formData.append('username', username);
    formData.append('email', email);
    formData.append('name', name);
    formData.append('password', password);

    return this.http.post(this.server + 'user/register', formData);
  }

  login(username: string, password: string) {
    return this.http.post<any>(this.server + 'user/login', { username: username, password: password })
      .pipe(map(user => {
                if (user && user.token) {
                  localStorage.setItem('currentUser', JSON.stringify(user));
                  return user;
                }

                return null;
    }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/home']);
  }

  isLoggedIn() {
    let currentUserJSON = localStorage.getItem('currentUser');
    if (!currentUserJSON) {
      return false;
    }

    var currentUser = JSON.parse(currentUserJSON);
    const helper = new JwtHelperService();
    return helper.isTokenExpired(currentUser.token);
  }

  isAdmin() {
    var currentUserJSON = localStorage.getItem('currentUser');
    if (!currentUserJSON) {
      return false;
    }

    var currentUser = JSON.parse(currentUserJSON);
    if (currentUser.admin == 0) {
      return false;
    }
   
    const helper = new JwtHelperService();
    return helper.isTokenExpired(currentUser.token);
  }

  getRegistrations() {
    return this.http.get<User[]>(this.server + 'admin/registrations');
  }

  approveRegistration(username: string) {
    return this.http.put(this.server + 'admin/registration/approve', {username: username});
  }

  disapproveRegistration(username: string) {
    return this.http.put(this.server + 'admin/registration/disapprove', {username: username});
  }

  getReports() {
    return this.http.get<User[]>(this.server + 'admin/reports');
  }

  approveReport(reporter: string, receiver: string) {
    return this.http.put(this.server + 'admin/report/approve', {reporter: reporter, receiver: receiver});
  }

  disapproveReport(reporter: string, receiver: string) {
    return this.http.put(this.server + 'admin/report/disapprove', {reporter: reporter, receiver: receiver});
  }

  report(reporter: string, receiver: string, reason: string) {
    return this.http.post(this.server + 'user/report', {reporter: reporter, receiver: receiver, reason: reason});
  }

  getReport(reporter: string, receiver: string) {
    return this.http.get<User>(this.server + 'user/report/' + reporter + '/' + receiver);
  }

  getMessages(username: string) {
    return this.http.get<Message[]>(this.server + 'user/messages/' + username);
  }

  openMessage(id: number) {
    return this.http.put(this.server + 'user/message', {id: id});
  }
}