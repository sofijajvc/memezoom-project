import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Meme, Reaction, Comment } from '../models/meme';
import { UserService } from './user.service';


@Injectable({
  providedIn: 'root'
})
export class MemeService {
  server: string = 'http://localhost:8000/';

  constructor(private http: HttpClient, 
    private userService: UserService) { }

  getUserMemes(username: string) : Observable<Meme[]> {
    return this.http.get<Meme[]>(this.server + 'meme/user/' + username);
  }

  getPopular() : Observable<Meme[]>{
  	return this.http.get<Meme[]>(this.server + 'meme/popular');
  }

  getRecent() : Observable<Meme[]>{
  	return this.http.get<Meme[]>(this.server + 'meme/recent');
  }

  getMeme(id: number) {
  	return this.http.get<Meme>(this.server + 'meme/' + id);
  }

  like(id: number) {
    var user = this.userService.getCurrentUser();
    return this.http.post(this.server + 'react/like', { username: user.username, meme: id });
  }

  dislike(id: number) {
    var user = this.userService.getCurrentUser();
    return this.http.post(this.server + 'react/dislike', { username: user.username, meme: id });
  }

  isLiked(id: number) {
    var user = this.userService.getCurrentUser();
    return this.http.get<Reaction>(this.server + 'react/' + id + '/' + user.username)
    .pipe(map(result => {
      if (result) {
        return result.react == 1;
      } else {
        return false;
      }
    }), catchError(err => of(false)));
  }

  isDisliked(id: number) {
    var user = this.userService.getCurrentUser();
    return this.http.get<Reaction>(this.server + 'react/' + id + '/' + user.username)
    .pipe(map(result => {
      if (result) {
        return result.react == -1;
      } else {
        return false;
      }
    }), catchError(err => of(false)));
  }

  unreact(id: number) {
    var user = this.userService.getCurrentUser();
    return this.http.delete(this.server + 'react/' + id + '/' + user.username);
  }

  getComments(id: number) {
    return this.http.get<Comment[]>(this.server + 'comment/' + id);
  }

  deleteComment(id: number) {
    return this.http.delete(this.server + 'comment/' + id);
  }

  leaveComment(id: number, username: string, content: string) {
    return this.http.post(this.server + 'comment', {meme: id, username: username, content: content});
  }

  getMemeLikes(id: number) {
    return this.http.get(this.server + 'react/meme/likes/' + id);
  }

  getMemeDislikes(id: number) {
    return this.http.get(this.server + 'react/meme/dislikes/' + id);
  }

  addMeme(title: string, file: File, creator: string) {
    var formData: FormData = new FormData();
    formData.append('image', file, file.name);
    formData.append('title', title);
    formData.append('creator', creator);
    return this.http.post(this.server + 'meme/', formData);
  }

  deleteMeme(id: number) {
    return this.http.delete(this.server + 'meme/' + id);
  }

  search(input: string) {
    return this.http.get<Meme[]>(this.server + 'meme/search/' + input);
  }

}