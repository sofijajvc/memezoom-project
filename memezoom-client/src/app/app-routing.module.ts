import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { UserGuard } from './guards/user.guard';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { MemePageComponent } from './components/memes/meme-page/meme-page.component';
import { ReportsComponent } from './components/admin/reports/reports.component';
import { RegistrationsComponent } from './components/admin/registrations/registrations.component';
import { SearchResultComponent } from './components/search-result/search-result.component';
import { ForbiddenAccessComponent } from './components/forbidden-access/forbidden-access.component';
import { InboxComponent } from './components/user/inbox/inbox.component';

const routes: Routes = [
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	{ path: 'home', component: HomeComponent },
	{ path: 'meme/:id', component: MemePageComponent },
	{ path: 'user/:username', component: ProfileComponent },
	{ path: 'user/inbox/:username', component: InboxComponent, canActivate: [AuthGuard, UserGuard] },
	{ path: 'admin/reports', component: ReportsComponent, canActivate: [RoleGuard], data: {expectedRole: 1} },
	{ path: 'admin/registrations', component: RegistrationsComponent, canActivate: [RoleGuard], data: {expectedRole: 1} },
	{ path: 'search/:input', component: SearchResultComponent, canActivate: [AuthGuard] },
	{ path: 'forbidden', component: ForbiddenAccessComponent },
	{ path: '**', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}