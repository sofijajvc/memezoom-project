import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators'
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl, FormGroupDirective, NgForm, ValidatorFn } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class ConfirmValidParentMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        return control.parent.invalid && control.touched;
    }
}

@Component({
	selector: 'app-register-dialog',
	templateUrl: './register-dialog.component.html',
	styleUrls: ['./register-dialog.component.css']
})
export class RegisterDialogComponent implements OnInit {

	confirmValidParentMatcher = new ConfirmValidParentMatcher();

	registerForm: FormGroup;
	submitted = false;
	errorMessage: string = "";
	selectedFile: File = null;

	formError = "Passwords don't match!";
	matching;

	constructor(
		public dialogRef: MatDialogRef<RegisterDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: string,
		private formBuilder: FormBuilder,
		private router: Router,
		private userService: UserService) {}

	onNoClick(): void {
		this.dialogRef.close();
	}

	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			username: ['', Validators.required],
			name: ['', Validators.required],
			email: ['', Validators.email],
			matchingPasswords: this.formBuilder.group({
				password: ['', Validators.required],
				confirmPassword: ['', Validators.required]
			}, { validator:this.matchValidator })
			
		});

		this.matching = this.registerForm.controls.matchingPasswords;
	}

	matchValidator(group: FormGroup) {
		if (group.controls['password'].value == group.controls['confirmPassword'].value) return null;
		return {
			mismatch: true
		} ;
	}

   // convenience getter for easy access to form fields
   get f() { return this.registerForm.controls; }

   onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

   onSubmit() {
   		this.submitted = true;

          // stop here if form is invalid
          if (this.registerForm.invalid) {
          	return;
          }

          this.userService.register(this.f.username.value, this.f.name.value, 
          	this.f.email.value, this.registerForm.controls.matchingPasswords['controls'].password.value,
          	this.selectedFile)
          .pipe(first())
          .subscribe(
          	data => {
          		this.dialogRef.close();
          		this.router.navigate(['/home']);
          	},
          	error => {
          		this.errorMessage = "Username already exists.";
          	});
      }
  }

