import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators'
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
	selector: 'app-login-dialog',
	templateUrl: './login-dialog.component.html',
	styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit {
	loginForm: FormGroup;
	errorMessage: string = "";

	constructor(
		public dialogRef: MatDialogRef<LoginDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: string,
		private formBuilder: FormBuilder,
		private router: Router,
		private userService: UserService) {}

	onNoClick(): void {
		this.dialogRef.close();
	}

	ngOnInit() {
		this.errorMessage = "";
		this.loginForm = this.formBuilder.group({
			username: ['', Validators.required],
			password: ['', [Validators.required]]
		});
	}

   // convenience getter for easy access to form fields
   get f() { return this.loginForm.controls; }

   onSubmit() {
        // stop here if form is invalid
        if (this.loginForm.invalid) {
        	return;
        }

        this.userService.login(this.f.username.value, this.f.password.value)
        .pipe(first())
        .subscribe(
        	data => {
        		this.dialogRef.close();
        		this.router.navigate(['/home']);
        	},
        	error => {
        		this.errorMessage = "Login failed.";
        	});
    }

}
