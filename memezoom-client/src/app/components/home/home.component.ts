import { Component, OnInit } from '@angular/core';
import { MemeService } from '../../services/meme.service';
import { Observable, of } from 'rxjs';
import { Meme } from '../../models/meme'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  popularMemes: Meme[];
  recentMemes: Meme[];

  constructor(private memeService: MemeService) { }

  ngOnInit() {
  	this.memeService.getPopular().subscribe(memes => this.popularMemes = memes);
  	this.memeService.getRecent().subscribe(memes => this.recentMemes = memes);
  }
}
