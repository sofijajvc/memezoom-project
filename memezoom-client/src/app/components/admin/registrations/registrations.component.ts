import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-registrations',
  templateUrl: './registrations.component.html',
  styleUrls: ['./registrations.component.css']
})
export class RegistrationsComponent implements OnInit {

displayedColumns: string[] = ['username', 'name', 'timestamp', 'options'];
dataSource;
@ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private userService: UserService) { }

  refresh() {
  	this.userService.getRegistrations().subscribe(registrations => {
  		this.dataSource = new MatTableDataSource<User>(registrations);
  		this.dataSource.paginator = this.paginator;
  	});
  }

  ngOnInit() {
  	this.refresh();
  }

  approveRegistration(username: string) {
  	this.userService.approveRegistration(username).subscribe(registration => {
  		this.refresh();
  	})
  }

  disapproveRegistration(username: string) {
  	this.userService.disapproveRegistration(username).subscribe(registration => {
  		this.refresh();
  	})
  }
}
