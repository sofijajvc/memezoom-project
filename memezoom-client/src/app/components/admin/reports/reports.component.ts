import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

displayedColumns: string[] = ['reporter', 'receiver', 'reason', 'options'];
dataSource;
@ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private userService: UserService) { }

  refresh() {
  	this.userService.getReports().subscribe(reports => {
  		this.dataSource = new MatTableDataSource<User>(reports);
  		this.dataSource.paginator = this.paginator;
  	});
  }

  ngOnInit() {
  	this.refresh();
  }

  approveReport(reporter: string, receiver: string) {
  	console.log(reporter, receiver);
  	this.userService.approveReport(reporter, receiver).subscribe(report => {
  		this.refresh();
  	})
  }

  disapproveReport(reporter: string, receiver: string) {
  	this.userService.disapproveReport(reporter, receiver).subscribe(report => {
  		this.refresh();
  	})
  }


}
