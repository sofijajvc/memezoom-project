import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../../services/user.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component'
import { RegisterDialogComponent } from '../register-dialog/register-dialog.component'
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  searchForm: FormGroup;

  constructor(
  	private userService: UserService, 
  	public dialog: MatDialog,
    private router: Router,
    private formBuilder: FormBuilder) { }

  openLoginDialog(): void {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '250px',
      data: ''
    });
  }

  openRegisterDialog(): void {
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      width: '250px',
      data: ''
    });
  }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      searchInput: ['', Validators.required]
    });
  }

  isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  isAdmin() {
    return this.userService.isAdmin();  
  }

  logout() {
    this.userService.logout();
  }

  openProfile() {
    var username = this.userService.getCurrentUser().username;
    this.router.navigate(['/user/' + username]);
  }

  get f() { return this.searchForm.controls; }

  search() {
    if (this.searchForm.invalid) {
          return;
    }

    this.router.navigate(['search/' + this.f.searchInput.value]);
  }

  home() {
    this.router.navigate(['/home']);
  }

  openRegistrations() {
    this.router.navigate(['admin/registrations']);
  }

  openReports() {
    this.router.navigate(['admin/reports']);
  }
}
