import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators'
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { MemeService } from '../../../services/meme.service';

@Component({
  selector: 'app-comment-dialog',
  templateUrl: './comment-dialog.component.html',
  styleUrls: ['./comment-dialog.component.css']
})
export class CommentDialogComponent implements OnInit {

	commentForm: FormGroup;
	meme: number;

  constructor(private memeService: MemeService, private userService: UserService,
  	public dialogRef: MatDialogRef<CommentDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any,
		private formBuilder: FormBuilder,
		private router: Router) {
  			this.meme = data.meme;
		 }

  ngOnInit() {
  	this.commentForm = this.formBuilder.group({
			content: ['', [Validators.required, Validators.maxLength(100)]],
		});
  }

     get f() { return this.commentForm.controls; }

   onSubmit() {
        // stop here if form is invalid
        if (this.commentForm.invalid) {
        	return;
        }

        const username = this.userService.getCurrentUser().username;
        this.memeService.leaveComment(this.meme, username, this.f.content.value)
        .pipe(first())
        .subscribe(
        	data => {
        		this.dialogRef.close();
        		this.router.navigate(['/meme/' + this.meme]);
        	},
        	error => {
        		this.dialogRef.close();
        		this.router.navigate(['/meme/' + this.meme]);
        	});
    }

    cancel() {
      this.dialogRef.close();
      this.router.navigate(['/meme/' + this.meme]);
    }

}
