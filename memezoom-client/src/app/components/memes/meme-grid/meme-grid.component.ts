import { Component, OnInit, Input } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Meme } from '../../../models/meme'
import { MemeService } from '../../../services/meme.service';

@Component({
  selector: 'app-meme-grid',
  templateUrl: './meme-grid.component.html',
  styleUrls: ['./meme-grid.component.css']
})
export class MemeGridComponent implements OnInit {
  @Input()
  memes: Meme[];
  breakpoint: number;
  @Input()
  isProfile: boolean;
  
  constructor() { }

  ngOnInit() {
  	this.breakpoint = (window.innerWidth <= 1000) ? ((window.innerWidth <= 700) ? 1 : 2) : 3;
  }

  onResize(event) {
  	this.breakpoint = (window.innerWidth <= 1000) ? ((window.innerWidth <= 700) ? 1 : 2) : 3;
  }
}
