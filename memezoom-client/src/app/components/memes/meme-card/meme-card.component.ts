import { Component, OnInit, Input } from '@angular/core';
import { Meme } from '../../../models/meme';
import { Router } from '@angular/router';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { MemeService } from '../../../services/meme.service';
import { UserService } from '../../../services/user.service';
import { NgZone, ChangeDetectorRef } from '@angular/core'


@Component({
  selector: 'app-meme-card',
  templateUrl: './meme-card.component.html',
  styleUrls: ['./meme-card.component.css']
})
export class MemeCardComponent implements OnInit {

@Input()
 meme: Meme;
 @Input()
 isProfile: boolean;
 userImage;

@Input()
  isLiked: boolean;
  isDisliked: boolean;
  likes: string;
  dislikes: string;
  server: string = 'http://localhost:8000/';

  constructor(private router: Router, private sanitizer: DomSanitizer, private memeService: MemeService,
    private userService: UserService, private ngZone: NgZone, private cdr: ChangeDetectorRef) { }

  isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  isCurrentUser() {
    if (this.isLoggedIn()) {
      return this.userService.getCurrentUser().username == this.meme.creator;
    } else {
      return false;
    }
  }

  refresh() {
    this.memeService.isLiked(this.meme.id).subscribe(liked => this.isLiked = liked);
       this.memeService.isDisliked(this.meme.id).subscribe(liked => this.isDisliked = liked);
       this.memeService.getMemeLikes(this.meme.id).subscribe(likes => this.likes = JSON.stringify(likes));
       this.memeService.getMemeDislikes(this.meme.id).subscribe(dislikes => this.dislikes = JSON.stringify(dislikes));
  }

  ngOnInit() {
    this.userImage = this.sanitizer.bypassSecurityTrustStyle(`url(${this.server + this.meme.user.image})`);
    if (this.isLoggedIn()) {
      this.refresh();
    }
  }

  openMeme(id: number) {
  	this.router.navigate(['/meme/' + id]);
  }

  like() {
    this.memeService.like(this.meme.id).subscribe(res =>  this.refresh());;
  }

  dislike() {
    this.memeService.dislike(this.meme.id).subscribe(res =>  this.refresh());
  }

  unreact() {
    this.memeService.unreact(this.meme.id).subscribe(res =>  this.refresh());
  }

  openProfile(username: string) {
    this.router.navigate(['/user/' + username]);
  }

  deleteMeme() {
    this.memeService.deleteMeme(this.meme.id).subscribe(res => this.router.navigate(['/user/' + this.meme.user.username]));
  }
}
