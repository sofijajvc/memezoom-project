import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Meme, Comment } from '../../../models/meme';
import { MemeService } from '../../../services/meme.service';
import { UserService } from '../../../services/user.service';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { CommentDialogComponent } from '../comment-dialog/comment-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-meme-page',
  templateUrl: './meme-page.component.html',
  styleUrls: ['./meme-page.component.css']
})
export class MemePageComponent implements OnInit {

  @Input() meme: Meme;
  @Input() comments: Comment[];

  userImage;
  isLiked: boolean;
  isDisliked: boolean;
  likes: string;
  dislikes: string;
  server: string = 'http://localhost:8000/';

  constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer, 
    public memeService: MemeService, private userService: UserService, 
    public dialog: MatDialog, private router: Router) { }

  refresh() {
    this.memeService.isLiked(this.meme.id).subscribe(liked => this.isLiked = liked);
    this.memeService.isDisliked(this.meme.id).subscribe(liked => this.isDisliked = liked);
    this.memeService.getMemeLikes(this.meme.id).subscribe(likes => this.likes = JSON.stringify(likes));
    this.memeService.getMemeDislikes(this.meme.id).subscribe(dislikes => this.dislikes = JSON.stringify(dislikes));
    this.memeService.getComments(this.meme.id).subscribe(comments => this.comments = comments);
  }

  ngOnInit() {
  	const id = +this.route.snapshot.paramMap.get('id');
    this.memeService.getMeme(id).subscribe(meme => {
      this.meme = meme;
      this.userImage = this.sanitizer.bypassSecurityTrustStyle(`url(${this.server + this.meme.user.image})`);
      if (this.isLoggedIn()) {
        this.refresh();
      } else {
        this.memeService.getComments(this.meme.id).subscribe(comments => {console.log(comments); this.comments = comments});
      }});
  }

  isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  like() {
    this.memeService.like(this.meme.id).subscribe(res =>  this.refresh());
  }

  dislike() {
    this.memeService.dislike(this.meme.id).subscribe(res =>  this.refresh());
  }

  unreact() {
    this.memeService.unreact(this.meme.id).subscribe(res =>  this.refresh());
  }

  currentUsername() {
    return this.userService.getCurrentUser().username;
  }

  deleteComment(id: number) {
    this.memeService.deleteComment(id).subscribe(res => this.refresh());
  }

  comment() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = "450px";
    dialogConfig.data = {
      meme: this.meme.id
    };

    let dialogRef = this.dialog.open(CommentDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(() => {
      this.ngOnInit();
    });
  }

  openProfile(username: string) {
    this.router.navigate(['/user/' + username]);
  }
}
