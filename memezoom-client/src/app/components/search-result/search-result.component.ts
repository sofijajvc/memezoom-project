import { Component, OnInit } from '@angular/core';
import { MemeService } from '../../services/meme.service';
import { Observable, of } from 'rxjs';
import { Meme } from '../../models/meme';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

results: Meme[];
breakpoint: number;
input: string;

  constructor(private route: ActivatedRoute, private memeService: MemeService) {
  	this.input = this.route.snapshot.paramMap.get('input');
  	this.route.params.subscribe(params => {
	  	this.input = params['input'];
      this.ngOnInit()});
   }

  ngOnInit() {
  	this.memeService.search(this.input).subscribe(memes => this.results = memes);
  	this.breakpoint = (window.innerWidth <= 1000) ? ((window.innerWidth <= 700) ? 1 : 2) : 3;
  }

  onResize(event) {
  	this.breakpoint = (window.innerWidth <= 1000) ? ((window.innerWidth <= 700) ? 1 : 2) : 3;
  }

}
