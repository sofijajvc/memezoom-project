import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { Message } from '../../../models/user';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {
  displayedColumns: string[] = ['sender', 'subject', 'timestamp'];
  dataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  username: string;

  constructor(private userService: UserService, private route: ActivatedRoute,
    public dialog: MatDialog) { }

   refresh() {
  	this.userService.getMessages(this.username).subscribe(messages => {
  		console.log(messages);
  		this.dataSource = new MatTableDataSource<Message>(messages);
  		this.dataSource.paginator = this.paginator;
  	});
  }

  ngOnInit() {
  	this.username = this.route.snapshot.paramMap.get('username');
  	this.refresh();
  }

  openMessage(id: number, sender: string, subject: string, content: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = "450px";
    dialogConfig.data = {
      subject: subject,
      sender: sender,
      content: content
    };

    let dialogRef = this.dialog.open(MessageDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(() => {
      console.log(id);
      this.userService.openMessage(id).subscribe(message => this.refresh());
    });
  }
}
