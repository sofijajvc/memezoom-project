import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators'
import { Router } from '@angular/router';
import { MemeService } from '../../../services/meme.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.css']
})
export class UploadDialogComponent implements OnInit {

	uploadForm: FormGroup;
	username: string;
  selectedFile: File = null;

  constructor(private memeService: MemeService, private userService: UserService,
		private formBuilder: FormBuilder, private router: Router, 
    public dialogRef: MatDialogRef<UploadDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  	this.username = data.username;
  }

  ngOnInit() {
  	this.uploadForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(1)]],
			image: ['', [Validators.required]],
		});
  }

  get f() { return this.uploadForm.controls; }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  upload() {
    if (this.uploadForm.invalid) {
      return;
    }

    this.memeService.addMeme(this.f.title.value, this.selectedFile, this.username).subscribe(result => 
      this.dialogRef.close()
    );
  }

  cancel() {
    this.dialogRef.close();
    this.router.navigate(['/user/' + this.username]);
  }
}
