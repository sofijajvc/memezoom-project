import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { Meme } from '../../../models/meme';
import { User } from '../../../models/user';
import { MemeService } from '../../../services/meme.service';
import { UserService } from '../../../services/user.service';
import { UploadDialogComponent } from '../upload-dialog/upload-dialog.component';
import { ReportDialogComponent } from '../report-dialog/report-dialog.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  server: string = 'http://localhost:8000/';
  user: User;
  userMemes: Meme[];
  userImage;
  username:string;
  isReportable: boolean;
  isReported: boolean;
  isBanned: boolean;

  constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer, private router: Router,
    public memeService: MemeService, private userService: UserService, public dialog: MatDialog) { }

  refresh() {
    this.userService.getUser(this.username).subscribe(user => {
      this.user = user;
      this.isBanned = user.banned == 1;
      this.userImage = this.sanitizer.bypassSecurityTrustStyle(`url(${this.server + this.user.image})`);
    });
    this.memeService.getUserMemes(this.username).subscribe(memes => {
      this.userMemes = memes 
    });
    this.checkIsReportable();
  }

  ngOnInit() {
  	this.username = this.route.snapshot.paramMap.get('username');
    this.refresh();
  }

  isLoggedIn() {
    return this.userService.isLoggedIn();
  }

  isCurrentUser() {
    if (this.isLoggedIn) {
      return this.userService.getCurrentUser().username == this.username;
    } else {
      return false;
    }
  }

  checkIsReportable() {
    if (this.isLoggedIn()) {
      if (!this.isCurrentUser()) {
        this.userService.getReport(this.userService.getCurrentUser().username, this.username).subscribe(res => {
          if (res) {
            this.isReported = true;
          } else {
            this.isReported = false;
          }

          this.isReportable = !this.isReported;
        });
      } else {
        this.isReportable = false;
        this.isReported = false;
      }
    } else {
      this.isReportable = false;
      this.isReported = false;
    }
  }

  openUploadDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = "500px";
    dialogConfig.data = {
      username: this.user.username
    };

    let dialogRef = this.dialog.open(UploadDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(() => {
      this.refresh();
    });
  }

  report() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = "450px";
    dialogConfig.data = {
      receiver: this.user.username,
      reporter: this.userService.getCurrentUser().username
    };

    let dialogRef = this.dialog.open(ReportDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(() => {
      this.refresh();
    });
  }

  messages() {
    this.router.navigate(['/user/inbox/' + this.username]);
  }
}
