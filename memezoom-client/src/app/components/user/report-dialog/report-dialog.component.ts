import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators'
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-report-dialog',
  templateUrl: './report-dialog.component.html',
  styleUrls: ['./report-dialog.component.css']
})
export class ReportDialogComponent implements OnInit {

  reportForm: FormGroup;
  reporter: string;
  receiver: string;

  constructor(private userService: UserService,
		private formBuilder: FormBuilder, private router: Router, 
    public dialogRef: MatDialogRef<ReportDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  	this.reporter = data.reporter;
  	this.receiver = data.receiver;
  }

  ngOnInit() {
  	this.reportForm = this.formBuilder.group({
      reason: ['', [Validators.required, Validators.minLength(1)]]
	});
  }

  get f() { return this.reportForm.controls; }


  submit() {
    if (this.reportForm.invalid) {
      return;
  	}

    this.userService.report(this.reporter, this.receiver, this.f.reason.value).subscribe(res => this.dialogRef.close());
  }

  cancel() {
    this.dialogRef.close();
    this.router.navigate(['/user/' + this.receiver]);
  }
}
