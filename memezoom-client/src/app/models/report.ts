export class Report {
	reporter: string;
	receiver: string;
	reason: string;
}