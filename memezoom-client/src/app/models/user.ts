export class Message {
	sender: string;
	subject: string;
	content: string;
	id: number;
	opened: number;
	receiver: string;
}

export class User {
	username: string;
	name: string;
	password: string;
	image: string;
	admin: number;
	registered: number;
	timestamp: string;
	banned: number;
	messages: Message[];
	email: string;
}