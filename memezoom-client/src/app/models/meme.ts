import { User } from './user';

export class Comment {
	user: User;
	content: string;
	timestamp: string;
	meme: number;
}

export class Reaction {
	user: User;
	meme_id: number;
	react: number;
}

export class Meme {
	title: string;
	image: string;
	likes: number;
	dislikes: number;
	creator: string;
	user: User;
	timestamp: string;
	id: number;
}