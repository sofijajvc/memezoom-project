const express = require('express');
const cors = require('cors');
var bodyParser = require('body-parser');
var jsonwebtoken = require("jsonwebtoken");
var user_routes = require('./routes/user.routes');
var meme_routes = require('./routes/meme.routes');
var comment_routes = require('./routes/comment.routes');
var reaction_routes = require('./routes/reaction.routes');
var admin_routes = require('./routes/admin.routes');

var app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/user', user_routes);
app.use('/meme', meme_routes);
app.use('/comment', comment_routes);
app.use('/react', reaction_routes);
app.use('/admin', admin_routes);

app.use(express.static('uploads'));

app.listen(8000, () => {
  console.log('Server started!');
});
