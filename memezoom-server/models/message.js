/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('message', {
    receiver: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    content: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: DataTypes.NOW
    },
    subject: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    opened: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '0'
    },
    sender: {
      type: DataTypes.STRING(50),
      allowNull: true,
      defaultValue: 'admin'
    }
  }, {
    tableName: 'message'
  });
};
