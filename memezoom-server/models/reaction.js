/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reaction', {
    username: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    meme_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    react: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'reaction'
  });
};
