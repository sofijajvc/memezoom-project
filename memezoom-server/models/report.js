/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('report', {
    reporter: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    receiver: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    reason: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: DataTypes.NOW
    }
  }, {
    tableName: 'report'
  });
};
