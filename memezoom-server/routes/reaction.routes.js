var express = require('express');
var router = express.Router();
const reactionController = require('../controllers/reactionController');

router.get('/meme/likes/:meme_id', reactionController.getMemeLikes);
router.get('/meme/dislikes/:meme_id', reactionController.getMemeDislikes);
router.post('/like', reactionController.likeMeme);
router.post('/dislike', reactionController.dislikeMeme);
router.get('/:id/:username', reactionController.getReaction);
router.delete('/:id/:username', reactionController.unreactToMeme);

module.exports = router;