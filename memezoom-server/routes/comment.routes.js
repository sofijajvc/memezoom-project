var express = require('express');
var router = express.Router();
const commentController = require('../controllers/commentController');

router.post('/', commentController.leaveComment);
router.get('/:id', commentController.getComments);
router.delete('/:id', commentController.deleteComment);

module.exports = router;