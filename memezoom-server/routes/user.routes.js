var express = require('express');
var router = express.Router();
const userController = require('../controllers/userController');

router.post('/register', userController.register);
router.post('/login', userController.login);
router.get('/:username', userController.getUser);
router.get('/report/:reporter/:receiver', userController.getReport);
router.post('/report', userController.addReport);
router.get('/messages/:username', userController.getMessages);
router.put('/message', userController.openMessage);

module.exports = router;