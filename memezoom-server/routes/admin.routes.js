var express = require('express');
var router = express.Router();
const adminController = require('../controllers/adminController');

router.get('/registrations', adminController.getRegistrations);
router.get('/reports', adminController.getReports);
router.put('/registration/approve', adminController.approveRegistration);
router.put('/registration/disapprove', adminController.approveRegistration);
router.put('/report/approve', adminController.approveReport);
router.put('/report/disapprove', adminController.disapproveReport);

module.exports = router;