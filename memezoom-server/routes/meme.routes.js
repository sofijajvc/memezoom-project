var express = require('express');
var router = express.Router();
const memeController = require('../controllers/memeController');

router.get('/recent', memeController.getRecent);
router.get('/popular', memeController.getPopular);
router.get('/search/:input', memeController.search);
router.get('/:id', memeController.getMeme);
router.get('/user/:username', memeController.getByUser);
router.delete('/:id', memeController.delete);
router.post('/', memeController.addMeme);

module.exports = router;