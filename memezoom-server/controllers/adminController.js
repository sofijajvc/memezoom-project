const Sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const user = require('../models/user.js');
const report = require('../models/report.js');
const message = require('../models/message.js');

const sequelize = new Sequelize('memezoom', 'root', '', {
	dialect: 'mysql',
	define: {
		timestamps: false
	}
});

const User = user(sequelize, Sequelize);
const Report = report(sequelize, Sequelize);
const Message = message(sequelize, Sequelize);

Report.belongsTo(User, {foreignKey: 'reporter', targetKey: 'username'});
Report.belongsTo(User, {foreignKey: 'receiver', targetKey: 'username'});
Message.belongsTo(User, {foreignKey: 'receiver', targetKey: 'username'});

Report.sync();
Message.sync();
User.sync();

module.exports.getRegistrations = function(req, res) {
	User.findAll({where: {registered: 0}, 
		order: [
            ['timestamp', 'DESC'],
        ]}).then(function(registrations) {
		return res.status(200).json(registrations);
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.approveRegistration = function(req, res) {
	User.findOne({where: {username: req.body.username}}).then(function(user) {
		user.registered = 1;
		user.save().then(function(user) {
			return res.status(200).json(user);
		}).catch(function(err) {
			return res.status(500).json(err);
		})
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.disapproveRegistration = function(req, res) {
	User.findOne({where: {username: req.body.username}}).then(function(user) {
		user.destroy().then(function(user) {
			return res.status(200).json(user);
		}).catch(function(err) {
			return res.status(500).json(err);
		})
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}


module.exports.getReports = function(req, res) {
	Report.findAll({order: [
            ['timestamp', 'DESC'],
        ]}).then(function(reports) {
		return res.status(200).json(reports);
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.approveReport = function(req, res) {
	Report.findOne({where: {receiver: req.body.receiver, reporter: req.body.reporter}}).then(function(report) {
		report.destroy().then(function(report) {
			var message = Message.build({receiver: req.body.receiver, subject: 'Warning', content: "You have been banned. You cannot upload memes or comment other uploads."});
			message.save().then(function(message) {
				var user = User.findOne({where: {username: req.body.receiver}}).then(function(user) {
					user.banned = 1;
					user.save().then(function(user) {
						return res.status(200).json(user);
					}).catch(function(err) {
						return res.status(500).json(err);
					})
				}).catch(function(err) {
					return res.status(500).json(err);
				})
			}).catch(function(err) {
				return res.status(500).json(err);
			});
		}).catch(function(err) {
			return res.status(500).json(err);
		})
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.disapproveReport = function(req, res) {
	Report.findOne({where: {receiver: req.body.receiver, reporter: req.body.reporter}}).then(function(report) {
		report.destroy().then(function(report) {
			var message = Message.build({receiver: req.body.reporter, subject: 'Report review', content: "Your report on " + req.body.receiver + " has been disapproved."});
			message.save().then(function(message) {
				return res.status(200).json(message);
			}).catch(function(err) {
				return res.status(500).json(err);
			});
		}).catch(function(err) {
			return res.status(500).json(err);
		})
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}