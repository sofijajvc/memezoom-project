const Sequelize = require('sequelize');
var multer = require('multer');
var path = require('path');
var fs = require('fs');
const jwt = require('jsonwebtoken');
const user = require('../models/user.js');
const meme = require('../models/meme.js');
const message = require('../models/message.js');
const report = require('../models/report.js');

const sequelize = new Sequelize('memezoom', 'root', '', {
	dialect: 'mysql',
	define: {
		timestamps: false
	}
});

const User = user(sequelize, Sequelize);
const Meme = meme(sequelize, Sequelize);
const Message = message(sequelize, Sequelize);
const Report = report(sequelize, Sequelize);

Report.belongsTo(User, {foreignKey: 'reporter', targetKey: 'username'});
Report.belongsTo(User, {foreignKey: 'receiver', targetKey: 'username'});
User.hasMany(Message, {as: 'messages', foreignKey: 'receiver'});

Message.sync();
Report.sync();
Meme.sync();
User.sync();

module.exports.getUser = function(req, res) {
	User.findOne({ where: {username: req.params.username} }).then(function(user) {
		if (!user) {
			return res.status(401).json({error: "Username doesn't exist."});
		} else {
			return res.status(200).json(user);
		}
	})
}

module.exports.login = function(req, res) {
	var user = User.findOne({
		where: {
			username: req.body.username
		}
	}).then(function(user) {
		if (!user) {
			return res.status(401).json({ error: 'Username does not exist.' });			
		}
		if (user.password != req.body.password) {
			return res.status(401).json({ error: 'Invalid password.' });
		} else {
			 return res.json({ username: user.username, name: user.name, admin: user.admin, token: jwt.sign({ username: user.username, name: user.name }, 'secret') });
		}
	}).catch(function(err) {
		return res.status(500).json(err);
	});
};

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

var upload = multer({storage: storage}).single('image');

module.exports.register = function(req, res) {
	upload(req, res, function(err) {
		var user = User.findOne({
			where: {
				username: req.body.username
			}
		}).then(function(user) {
			if (!user) {
				User.build({ username: req.body.username, name: req.body.name, email: req.body.email, password:req.body.password, image: req.file.filename })
				.save()
				.then(newUser => {
					res.status(200).json({
						success: 'New user has been created.'
					})
				})
				.catch(error => {
					res.status(500).json({
						error: error
					})
				});
			} else {
				res.status(401).json({
					error: 'Username already exists!'
				})
			}
		})
	})
};

module.exports.getReport = function(req, res) {
	Report.findOne({where: {reporter: req.params.reporter, receiver: req.params.receiver}}).then(function(report) {
		if (report) {
			return res.status(200).json(report);
		} else {
			return res.status(200).json(null)
		}
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.addReport = function(req, res) {
	var report = Report.build({reporter: req.body.reporter, receiver: req.body.receiver, reason: req.body.reason});
	report.save().then(function(report) {
		return res.status(200).json(report);
	}).catch(function(err) {
		return res.status(500).json(err);
	})
}

module.exports.getMessages = function(req, res) {
	Message.findAll({where: {receiver: req.params.username}}).then(function(messages) {
		return res.status(200).json(messages);
	}).catch(function(error) {
		return res.status(500).json(error);
	});
}

module.exports.openMessage = function(req, res) {
	Message.findOne({where: {id: req.body.id}}).then(function(message) {
		if (message) {
			message.opened = 1;
			message.save().then(function(message) {
				return res.status(200).json(message);
			}).catch(function(error) {
				return res.status(500).json(error);
			})
		} else {
			return res.status(500).json({error: 'Not found.'});
		}
		
	}).catch(function(error) {
		return res.status(500).json(error);
	});
}