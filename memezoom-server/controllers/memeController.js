const Sequelize = require('sequelize');
var multer = require('multer');
var path = require('path');
var fs = require('fs');
const meme = require('../models/meme.js');
const user = require('../models/user.js');
const reaction = require('../models/reaction.js');
const comment = require('../models/comment.js');

const sequelize = new Sequelize('memezoom', 'root', '', {
	dialect: 'mysql',
	define: {
		timestamps: false
	}
});

sequelize.sync();

const Meme = meme(sequelize, Sequelize);
const User = user(sequelize, Sequelize);
const Reaction = reaction(sequelize, Sequelize);
const Comment = comment(sequelize, Sequelize);

Meme.belongsTo(User, {foreignKey: 'creator', targetKey: 'username'});
Meme.hasMany(Reaction, {as: 'reactions', foreignKey: 'meme_id'});

Reaction.sync();
Comment.sync();
Meme.sync();
User.sync();

module.exports.getByUser = function(req, res) {
	Meme.findAll({where: {creator: req.params.username}, include: [User]}).then(function(memes) {
		return res.status(200).json(memes);
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.getPopular = function(req, res) {
	Meme.findAll({
		include: [ User ],
	    order: [[sequelize.literal('(SELECT IFNULL(SUM(Reaction.react), 0) FROM Reaction WHERE Reaction.meme_id = meme.id)'), 'DESC']]
	}).then(function(memes) {
		return res.status(200).json(memes);
	}).catch(function(err) {
		return res.status(500).json(err);
	});
};

module.exports.getRecent = function(req, res) {
	Meme.findAll({ include: [ User ], 
		order: [
            ['timestamp', 'DESC'],
        ], }).then(function(memes) {
		return res.status(200).json(memes);
	}).catch(function(err) {
		return res.status(500).json(err);
	});
};

module.exports.getMeme = function(req, res) {
	Meme.findOne({ where: { id: req.params.id },  include: [User] }).then(function(meme) {
		return res.status(200).json(meme);
	}).catch(function(err) {
		return res.status(500).json(err);
	});
};

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

var upload = multer({storage: storage}).single('image');

module.exports.addMeme = function(req, res) {
	upload(req, res, function(err) {
		if (err) {
			console.log('patkaaaaaaaa');
			return res.status(500).json(err);
		} else {
			const meme = Meme.build({title: req.body.title, creator: req.body.creator, image: req.file.filename });
			meme.save().then(function(meme) {
				return res.status(200).json(meme);
			}).catch(function(err) {
				console.log(err);
				return res.status(500).json(err);
			});
		}
	});
}

module.exports.delete = function(req, res) {
	Meme.findOne({ where: { id: req.params.id }}).then(function(meme) {
		var image = meme.image;
		Meme.destroy({where: {id: req.params.id}}).then(function(result) {
			var filePath = './uploads/' + image; 
			fs.unlinkSync(filePath);
			return res.status(200).json();
		}).catch(function(err) {
			return res.status(500).json(err);
		});
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.search = function(req, res) {
	Meme.findAll({where: { title: { $like: '%' + req.params.input + '%'}}, include: [User]}).then(function(memes) {
		return res.status(200).json(memes);
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}