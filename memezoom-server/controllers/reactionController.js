const Sequelize = require('sequelize');
const meme = require('../models/meme.js');
const user = require('../models/user.js');
const reaction = require('../models/reaction.js');

const sequelize = new Sequelize('memezoom', 'root', '', {
	dialect: 'mysql',
	define: {
		timestamps: false
	}
});
sequelize.sync();

const Meme = meme(sequelize, Sequelize);
const User = user(sequelize, Sequelize);
const Reaction = reaction(sequelize, Sequelize);

Reaction.belongsTo(User, {foreignKey: 'username', targetKey: 'username'});
Reaction.belongsTo(Meme, {foreignKey: 'meme_id', targetKey: 'id'});

Reaction.sync();
Meme.sync();
User.sync();

reactToMeme = function(req, res, like) {
	Meme.findOne({where: { id: req.body.meme }}).then(function(meme) {
		User.findOne({where: {username: req.body.username}}).then(function(user) {
			Reaction.findAndCountAll({where: {username: req.body.username, meme_id: req.body.meme}}).then(function(count) {
				if (count.count == 0) {
					var reaction = Reaction.build({meme_id: req.body.meme, username: req.body.username, react: like});
					reaction.save().then(function() {
						return res.status(200).json(reaction);
					}).catch(function(err) {
						return res.status(500).json(err);
					}) 
				} else {
					return res.status(500).json({error: "Already reacted."});
				}
			})
		}).catch(function(err) {
			return res.status(500).json(err);
		})
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.likeMeme = function(req, res) {
	return reactToMeme(req, res, 1);
}

module.exports.dislikeMeme = function(req, res) {
	return reactToMeme(req, res, -1);
}

module.exports.getReaction = function(req, res) {
	Reaction.findOne({where: {meme_id: req.params.id, username: req.params.username}}).then(function(reaction) {
		if (reaction) {
			return res.status(200).json(reaction);
		} else {
			return res.status(200).json(reaction);
		}
	}).catch(function(err) {
		return res.status(500).json(err);
	})
}

module.exports.unreactToMeme = function(req, res) {
	Reaction.findOne({where: {username: req.params.username, meme_id: req.params.id}}).then(function(reaction) {
		if(reaction) {

			Reaction.destroy({where: {username: req.params.username, meme_id: req.params.id}}).then((result) => {
				return res.status(200).json();
			}).catch((err) => {
				return res.status(500).json(err);
			});
		}
		else {
			return res.status(500).json({error: "Reaction doesn't exist."});
		}
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

getMemeReactions = function(req, res, like_value) {
	Reaction.findAndCountAll({where: { meme_id: req.params.meme_id, react: like_value}}).then(function(result) {
		return res.status(200).json(result.count);
	}).catch(function(err) {
		return res.status(200).json(0);
	});
}

module.exports.getMemeLikes = function(req, res) {
	return getMemeReactions(req, res, 1);
}

module.exports.getMemeDislikes = function(req, res) {
	return getMemeReactions(req, res, -1);
}