const Sequelize = require('sequelize');
const meme = require('../models/meme.js');
const user = require('../models/user.js');
const comment = require('../models/comment.js');

const sequelize = new Sequelize('memezoom', 'root', '', {
	dialect: 'mysql',
	define: {
		timestamps: false
	}
});
sequelize.sync();

const Meme = meme(sequelize, Sequelize);
const User = user(sequelize, Sequelize);
const Comment = comment(sequelize, Sequelize);

Comment.belongsTo(User, {foreignKey: 'username', targetKey: 'username'});
Comment.belongsTo(Meme, {foreignKey: 'meme_id', targetKey: 'id'});

Comment.sync();
Meme.sync();
User.sync();

module.exports.getComments = function(req, res) {
	Comment.findAll({where: {meme_id: req.params.id}, include: [User]}).then(function(comments) {
		return res.status(200).json(comments);
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}

module.exports.leaveComment = function(req, res) {
	var comment = Comment.build({meme_id: req.body.meme, username: req.body.username, 
		content: req.body.content });
	comment.save().then(function() {
		return res.status(200).json(comment);
	}).catch(function(err) {
		return res.status(500).json(err);
	}) 
}

module.exports.deleteComment = function(req, res) {
	Comment.findOne({where: {id: req.params.id}}).then(function(comment) {
		if (comment) {
			comment.destroy({force: true}).then(function(result) {
				return res.status(200);
			}).catch(function(err) {
				return res.status(500).json(err);
			});
		} else {
			return res.status(500).json({error: "Comment doesn't exist!"});
		}
	}).catch(function(err) {
		return res.status(500).json(err);
	});
}